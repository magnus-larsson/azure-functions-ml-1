Build instructions

```
func init LocalFunctionProj --typescript
cd LocalFunctionProj
func new --name ML1HttpExample --template "HTTP trigger" --authlevel "anonymous"
npm install
npm start

az group create --name AzureFunctionsQuickstart-rg --location westeurope
az storage account create --name ml1sa --location westeurope --resource-group AzureFunctionsQuickstart-rg --sku Standard_LRS
az functionapp create --resource-group AzureFunctionsQuickstart-rg --consumption-plan-location westeurope --runtime node --runtime-version 14 --functions-version 4 --name ml1app --storage-account ml1sa
npm run build:production
func azure functionapp publish ml1app
func azure functionapp logstream ml1app
func azure functionapp list-functions ml1app --show-keys
```
